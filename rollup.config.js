export default {
  input: 'build/main.js',
  output: {
    file: 'dist/odv.js',
    format: 'iife',
    name: 'odv'
  },
  context: 'window'
}
