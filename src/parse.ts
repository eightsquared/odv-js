import { RawAttributes, RawElement, RootElement } from './elements';
import { warn } from './util';

export function parse(src: string | RawElement): RootElement {
  let el: RawElement = <RawElement>src;
  if (typeof src === 'string') {
    if (typeof DOMParser === 'undefined') {
      throw new Error('parse: unable to parse XML string: DOMParser not defined');
    }
    const parser = new DOMParser();
    const doc = parser.parseFromString(src, 'application/xml')
    const root = doc.documentElement;
    if (!root || root.localName !== 'odv') {
      throw new Error('parse: unable to parse XML string: XML is malformed');
    }
    el = parseElement(root);
  }
  if (typeof el !== 'object') {
    throw new Error('parse: unable to parse source document: source is not an object');
  }
  return new RootElement(el);
}

function parseElement(src: Element): RawElement {
  const el: RawElement = {
    name: src.nodeName,
  };
  if (typeof src.attributes !== 'undefined') {
    const attrs: RawAttributes = {};
    let hasAttrs = false;
    for (let i = 0; i < src.attributes.length; i++) {
      const node = src.attributes[i];
      if (typeof node.nodeValue === 'string') {
        attrs[node.nodeName] = node.nodeValue;
        hasAttrs = true;
      }
    }
    if (hasAttrs) {
      el.attributes = attrs;
    }
  }
  if (typeof src.childNodes !== 'undefined') {
    const children: RawElement[] = [];
    for (let i = 0; i < src.childNodes.length; i++) {
      const child = <Element>src.childNodes[i];
      switch (child.nodeType) {
        case Node.ELEMENT_NODE:
          children.push(parseElement(child));
          break;
        case Node.CDATA_SECTION_NODE:
          el.content = (child.nodeValue || '').trim();
          break;
      }
    }
    if (children.length > 0) {
      el.children = children;
    }
  }
  return el;
}
