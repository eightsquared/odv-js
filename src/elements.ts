import { Context } from './context';
import { Target, Color } from './targets';
import { clamp, warn } from './util';
import * as csv from './csv';

export interface RawElement {
  name: string;
  attributes?: RawAttributes;
  children?: RawElement[];
  content?: any;
}

export interface RawAttributes {
  [name: string]: string;
}

interface Schema {
  [name: string]: SchemaNode;
}

type SchemaNode = [NodeConstructorFunc, Schema];

interface NodeConstructorFunc {
  new(src: RawElement): BaseElement;
}

abstract class BaseElement {
  parent: BaseElement | null = null;

  addChild(child: BaseElement) {
    child.parent = this;
  }

  draw(dst: Target, ctx: Context) { }
}

export class RootElement extends BaseElement {
  paint: PaintElement[] = [];
  data: DataElement[] = [];
  units: UnitsElement[] = [];
  children: BaseElement[] = [];

  constructor(src: RawElement) {
    super();
    this.parse(this, schema, src.children);
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    } else if (child instanceof DataElement) {
      this.data.push(child);
    } else if (child instanceof UnitsElement) {
      this.units.push(child);
    } else {
      this.children.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const width = dst.width();
    const height = dst.height();
    const rootCtx = new Context(ctx);
    rootCtx.define('pctx', function(stack) {
      stack.push(width * (stack.pop() / 100));
    });
    rootCtx.define('pcty', function(stack) {
      stack.push(height * (stack.pop() / 100));
    });
    rootCtx.define('pctmin', function(stack) {
      stack.push((width < height ? width : height) * (stack.pop() / 100));
    });
    rootCtx.define('pctmax', function(stack) {
      stack.push((width > height ? width : height) * (stack.pop() / 100));
    });
    for (let i = 0; i < this.data.length; i++) {
      for (let j = 0; j < this.data[i].series.length; j++) {
        this.data[i].series[j].defineOperators(rootCtx);
      }
    }
    for (let i = 0; i < this.units.length; i++) {
      for (let j = 0; j < this.units[i].units.length; j++) {
        this.units[i].units[j].defineOperators(rootCtx);
      }
    }
    if (this.paint.length > 0) {
      dst.begin();
      dst.move(0, 0);
      dst.line(width, 0);
      dst.line(width, height);
      dst.line(0, height);
      dst.close();
      for (let i = 0; i < this.paint.length; i++) {
        this.paint[i].draw(dst, rootCtx);
      }
      dst.end();
    }
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].draw(dst, rootCtx);
    }
  }

  series(name: string): SeriesElement | null {
    for (let i = 0; i < this.data.length; i++) {
      for (let j = 0; j < this.data[i].series.length; j++) {
        const series = this.data[i].series[j];
        if (series.name === name) {
          return series;
        }
      }
    }
    return null;
  }

  private parse(node: BaseElement, schema: Schema, children: RawElement[] = []) {
    for (let i = 0; i < children.length; i++) {
      const child = children[i];
      if (child.name in schema) {
        const childSchema = schema[child.name];
        const ctor = childSchema[0];
        const childNode = new ctor(child);
        node.addChild(childNode);
        this.parse(childNode, childSchema[1], child.children);
      }
    }
  }
}

class ArcElement extends BaseElement {
  paint: BaseElement[] = [];
  x: string;
  y: string;
  r1: string;
  r2: string;
  a1: string;
  a2: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x = '' + (attrs.x || '');
    this.y = '' + (attrs.y || '');
    this.r1 = '' + (attrs.r1 || '');
    this.r2 = '' + (attrs.r2 || '');
    this.a1 = '' + (attrs.a1 || '');
    this.a2 = '' + (attrs.a2 || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const x = ctx.eval(this.x);
    if (isNaN(x)) return;
    const y = ctx.eval(this.y);
    if (isNaN(y)) return;
    const r1 = ctx.eval(this.r1);
    if (isNaN(r1)) return;
    const r2 = ctx.eval(this.r2);
    if (isNaN(r2)) return;
    const a1 = ctx.eval(this.a1);
    if (isNaN(a1)) return;
    const a2 = ctx.eval(this.a2);
    if (isNaN(a2)) return;
    let startAngle = clamp(a1 % 1.0, 0, 1);
    let endAngle = clamp(a2 % 1.0, 0, 1);
    if (endAngle < startAngle) endAngle += 1;
    dst.begin();
    if (startAngle === endAngle) {
      dst.arc(x, y, r1, 0, 2 * Math.PI);
      dst.close()
      if (r2 > 0) {
        dst.move(x + r2, y);
        dst.arc(x, y, r2, 0, 2 * Math.PI);
        dst.close()
      }
    } else {
      startAngle = (startAngle * (2 * Math.PI)) - (0.5 * Math.PI);
      endAngle = (endAngle * (2 * Math.PI)) - (0.5 * Math.PI);
      dst.arc(x, y, r1, startAngle, endAngle);
      if (r2 > 0) {
        dst.line(x + (r2 * Math.cos(endAngle)), y + (r2 * Math.sin(endAngle)));
        dst.arc(x, y, r2, endAngle, startAngle);
      } else {
        dst.line(x, y)
      }
      dst.close()
    }
    for (let i = 0; i < this.paint.length; i++) {
      this.paint[i].draw(dst, ctx);
    }
    dst.end();
  }
}

class ClipElement extends BaseElement {
  children: BaseElement[] = [];
  x1: string;
  y1: string;
  x2: string;
  y2: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x1 = '' + (attrs.x1 || '');
    this.y1 = '' + (attrs.y1 || '');
    this.x2 = '' + (attrs.x2 || '');
    this.y2 = '' + (attrs.y2 || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    this.children.push(child);
  }

  draw(dst: Target, ctx: Context) {
    const x1 = ctx.eval(this.x1);
    if (isNaN(x1)) return; // TODO: warn
    const y1 = ctx.eval(this.y1);
    if (isNaN(y1)) return; // TODO: warn
    const x2 = ctx.eval(this.x2);
    if (isNaN(x2)) return; // TODO: warn
    const y2 = ctx.eval(this.y2);
    if (isNaN(y2)) return; // TODO: warn
    dst.begin();
    dst.move(x1, y1);
    dst.line(x2, y1);
    dst.line(x2, y2);
    dst.line(x1, y2);
    dst.close();
    dst.clip();
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].draw(dst, ctx);
    }
    dst.end();
  }
}

class ColorElement extends BaseElement {
  r: string;
  g: string;
  b: string;
  h: string;
  s: string;
  l: string;
  a: string;
  offset: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.r = '' + (attrs.r || '');
    this.g = '' + (attrs.g || '');
    this.b = '' + (attrs.b || '');
    this.h = '' + (attrs.h || '');
    this.s = '' + (attrs.s || '');
    this.l = '' + (attrs.l || '');
    this.a = '' + (attrs.a || '');
    this.offset = '' + (attrs.offset || '');
  }

  color(ctx: Context): Color {
    const c: Color = {
      r: 0,
      g: 0,
      b: 0,
      a: 0,
      offset: 0
    };
    let n: number;
    if (this.r || this.g || this.b) {
      c.a = 1;
      n = ctx.eval(this.r);
      if (!isNaN(n)) c.r = clamp(n, 0, 1);
      n = ctx.eval(this.g);
      if (!isNaN(n)) c.g = clamp(n, 0, 1);
      n = ctx.eval(this.b);
      if (!isNaN(n)) c.b = clamp(n, 0, 1);
    } else if (this.h || this.s || this.l) {
      c.a = 1;
      // https://drafts.csswg.org/css-color/#hsl-to-rgb
      let h = 0;
      let s = 0;
      let l = 0;
      n = ctx.eval(this.h);
      if (!isNaN(n)) h = 6 * n;
      while (h < 0) h += 6;
      while (h >= 6) h -= 6;
      n = ctx.eval(this.s);
      if (!isNaN(n)) s = clamp(n, 0, 1);
      n = ctx.eval(this.l);
      if (!isNaN(n)) l = clamp(n, 0, 1);
      const t2 = (l <= 0.5 ? l * (s + 1) : l + s - (l * s));
      const t1 = l*2 - t2;
      c.r = ColorElement.hueToRGBComponent(t1, t2, h+2);
      c.g = ColorElement.hueToRGBComponent(t1, t2, h);
      c.b = ColorElement.hueToRGBComponent(t1, t2, h-2);
    }
    if (this.a) {
      n = ctx.eval(this.a);
      if (!isNaN(n)) c.a = clamp(n, 0, 1);
		}
    if (this.offset) {
      n = ctx.eval(this.offset);
      if (!isNaN(n)) c.offset = clamp(n, 0, 1);
		}
    return c;
	}

  static hueToRGBComponent(t1: number, t2: number, h: number): number {
    if (h < 0) h += 6;
    if (h >= 6) h -= 6;
    if (h < 1) return (t2 - t1) * h + t1;
    if (h < 3) return t2;
    if (h < 4) return (t2 - t1) * (4 - h) + t1;
    return t1;
  }
}

class DataElement extends BaseElement {
  series: SeriesElement[] = [];

  constructor(src: RawElement) {
    super();
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof SeriesElement) {
      this.series.push(child);
    }
  }
}

class FillElement extends BaseElement {
  colors: ColorElement[] = [];
  x1: string;
  y1: string;
  x2: string;
  y2: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x1 = '' + (attrs.x1 || '');
    this.y1 = '' + (attrs.y1 || '');
    this.x2 = '' + (attrs.x2 || '');
    this.y2 = '' + (attrs.y2 || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof ColorElement) {
      this.colors.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const x1 = ctx.eval(this.x1);
    if (isNaN(x1)) return;
    const y1 = ctx.eval(this.y1);
    if (isNaN(y1)) return;
    const x2 = ctx.eval(this.x2);
    if (isNaN(x2)) return;
    const y2 = ctx.eval(this.y2);
    if (isNaN(y2)) return;
    dst.fill({
      colors: this.colors.map((c) => c.color(ctx)),
      x1: x1,
      y1: y1,
      x2: x2,
      y2: y2
    });
  }
}

class IfElement extends BaseElement {
  test: string;
  clauses: IfClauseElement[] = [];

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.test = '' + (attrs.test || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof IfClauseElement) {
      this.clauses.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const value = ctx.eval(this.test);
    for (let i = 0; i < this.clauses.length; i++) {
      const clause = this.clauses[i];
      if (clause.pass(value, ctx)) clause.draw(dst, ctx);
    }
  }
}

class IfClauseElement extends BaseElement {
  children: BaseElement[] = [];
  inclusive: string;
  max: string;
  min: string;

  private readonly test: (value: number, min: number, max: number, inclusive: string) => boolean;

  static tests: { [kind: string]: (value: number, min: number, max: number, inclusive: string) => boolean; } = {
    "between": (value, min, max, inclusive) => {
      if (isNaN(max)) return false;
      if (isNaN(min)) return false;
      if (inclusive === 'max') return min < value && value <= max;
      if (inclusive === 'min') return min <= value && value < max;
      if (inclusive === 'both') return min <= value && value <= max;
      return min < value && value < max;
    },
    "defined": (value, min, max, inclusive) => {
      return !isNaN(value);
    },
    "equal": (value, min, max, inclusive) => {
      if (isNaN(min)) return false;
      return Math.abs(value - min) < 0.000001;
    },
    "greater": (value, min, max, inclusive) => {
      if (isNaN(min)) return false;
      if (inclusive === 'yes') return value >= min;
      return value > min;
    },
    "less": (value, min, max, inclusive) => {
      if (isNaN(max)) return false;
      if (inclusive == 'yes') return value <= max;
      return value < max;
    },
    "undefined": (value, min, max, inclusive) => {
      return isNaN(value);
    }
  };

  constructor(src: RawElement) {
    super();
    this.test = IfClauseElement.tests[src.name] || ((value, min, max, inclusive) => false);
    const attrs: RawAttributes = src.attributes || {};
    this.inclusive = '' + (attrs.inclusive || '');
    if (src.name === 'between') {
      this.max = '' + (attrs.max || '');
      this.min = '' + (attrs.min || '');
    } else if (src.name === 'equal') {
      this.max = '' + (attrs.to || '');
      this.min = this.max;
    } else if (src.name === 'greater') {
      this.min = '' + (attrs.than || '');
    } else if (src.name === 'less') {
      this.max = '' + (attrs.than || '');
    }
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    this.children.push(child);
  }

  pass(value: number, ctx: Context) {
    const max = ctx.eval(this.max);
    const min = ctx.eval(this.min);
    return this.test(value, min, max, this.inclusive);
  }

  draw(dst: Target, ctx: Context) {
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].draw(dst, ctx);
    }
  }
}

class LabelElement extends BaseElement {
  paint: PaintElement[] = [];
  text: string;
  expr: string;
  x: string;
  y: string;
  align: string;
  font: string;
  rotate: string;
  size: string;
  style: string;
  weight: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.text = '' + (attrs.text || '');
    this.expr = '' + (attrs.expr || '');
    this.x = '' + (attrs.x || '');
    this.y = '' + (attrs.y || '');
    this.align = '' + (attrs.align || '');
    this.font = '' + (attrs.font || '');
    this.rotate = '' + (attrs.rotate || '');
    this.size = '' + (attrs.size || '');
    this.style = '' + (attrs.style || '');
    this.weight = '' + (attrs.weight || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const x = ctx.eval(this.x);
    if (isNaN(x)) return void warn(`text: expression evaluates to NaN: x`);
    const y = ctx.eval(this.y);
    if (isNaN(y)) return void warn(`text: expression evaluates to NaN: y`);
    const rotate = ctx.eval(this.rotate);
    if (isNaN(rotate)) return void warn(`text: expression evaluates to NaN: rotate`);
    const size = ctx.eval(this.size);
    if (isNaN(size)) return void warn(`text: expression evaluates to NaN: size`);
    let s: string;
    if (this.expr.length > 0) {
      const expr = ctx.eval(this.expr);
      if (isNaN(expr)) return void warn(`text: expression evaluates to NaN: expr`);
      s = '' + expr;
    } else {
      s = ctx.sub(this.text);
    }
    const font = this.font || 'sans-serif';
    const weight = parseInt(this.weight);
    let align: 'center' | 'left' | 'right' = 'center';
    if (this.align === 'left' || this.align === 'right') align = this.align;
    let style: 'normal' | 'italic' | 'oblique' = 'normal';
    if (this.style === 'italic' || this.style === 'oblique') style = this.style;
    dst.begin();
    dst.text(s, x, y, align, font, size, style, weight, rotate * 2 * Math.PI);
    for (let i = 0; i < this.paint.length; i++) {
      this.paint[i].draw(dst, ctx);
    }
    dst.end();
    return;
  }
}

class LineElement extends BaseElement {
  paint: PaintElement[] = [];
  x1: string;
  y1: string;
  x2: string;
  y2: string;
  extend: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x1 = '' + (attrs.x1 || '');
    this.y1 = '' + (attrs.y1 || '');
    this.x2 = '' + (attrs.x2 || '');
    this.y2 = '' + (attrs.y2 || '');
    this.extend = '' + (attrs.extend || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    let x1 = ctx.eval(this.x1);
    if (isNaN(x1)) return;
    let y1 = ctx.eval(this.y1);
    if (isNaN(y1)) return;
    let x2 = ctx.eval(this.x2);
    if (isNaN(x2)) return;
    let y2 = ctx.eval(this.y2);
    if (isNaN(y2)) return;
    dst.begin();
    dst.move(x1, y1);
    dst.line(x2, y2);
    for (let i = 0; i < this.paint.length; i++) {
      this.paint[i].draw(dst, ctx);
    }
    dst.end();
  }
}

class LoopElement extends BaseElement {
  children: BaseElement[] = [];

  private series: string;
  private i = 0;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.series = '' + (attrs.series || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    this.children.push(child);
  }

  draw(dst: Target, ctx: Context) {
    let node: BaseElement = this;
    while (node.parent !== null) {
      node = node.parent;
      if (node instanceof LoopElement) return void warn('loop: nested loop elements not supported');
    }
    if (!(node instanceof RootElement)) return void warn('loop: unable to locate root element');
    const name = this.series.trim();
    const series = (<RootElement>node).series(name);
    if (series === null) return void warn(`loop: unknown series: ${name}`);
    if (series.values.length === 0) return void warn(`loop: series empty: ${name}`);
    const loopCtx = new Context(ctx);
    loopCtx.define(`#i`, (stack) => {
      stack.push(this.i);
    });
    for (let field in series.fields) {
      this.defineFieldOperator(loopCtx, series, field);
    }
    for (this.i = 0; this.i < series.values[0].length; this.i++) {
      for (let j = 0; j < this.children.length; j++) {
        for (let field in series.fields) {
          loopCtx.define(`$${field}`, series.raw[series.fields[field]][this.i]);
        }
        this.children[j].draw(dst, loopCtx);
      }
    }
    return;
  }

  private defineFieldOperator(ctx: Context, series: SeriesElement, field: string) {
    const fi = series.fields[field];
    ctx.define(`#${field}`, (stack) => {
      stack.push(series.values[fi][this.i]);
    });
    ctx.define(`#${field}:last`, (stack) => {
      for (let vi = this.i - 1; vi >= 0; vi--) {
        if (!isNaN(series.values[fi][vi])) {
          stack.push(series.values[fi][vi]);
          return;
        }
      }
      stack.push(NaN);
    });
    ctx.define(`#${field}:prev`, (stack) => {
      if (this.i > 0 && this.i <= series.values[fi].length) {
        stack.push(series.values[fi][this.i - 1]);
        return;
      }
      stack.push(NaN);
    });
  }
}

class PaintElement extends BaseElement {
  children: BaseElement[] = [];

  constructor(src: RawElement) {
    super();
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    this.children.push(child);
  }

  draw(dst: Target, ctx: Context) {
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].draw(dst, ctx);
    }
  }
}

class PathElement extends BaseElement {
  paint: PaintElement[] = [];
  children: BaseElement[] = [];

  constructor(src: RawElement) {
    super();
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    } else {
      this.children.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    dst.begin();
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].draw(dst, ctx);
    }
    for (let i = 0; i < this.paint.length; i++) {
      this.paint[i].draw(dst, ctx);
    }
    dst.end();
  }
}

class PathCmdElement extends BaseElement {
  x: string;
  y: string;
  c1x: string;
  c1y: string;
  c2x: string;
  c2y: string;

  static drawFns: { [kind: string]: (dst: Target, ctx: Context) => void; } = {
    "close": function(this: PathCmdElement, dst, ctx) {
      dst.close();
    },
    "cube": function(this: PathCmdElement, dst, ctx) {
      const x = ctx.eval(this.x);
      if (isNaN(x)) return; // TODO: warn
      const y = ctx.eval(this.y);
      if (isNaN(y)) return; // TODO: warn
      const c1x = ctx.eval(this.c1x);
      if (isNaN(c1x)) return; // TODO: warn
      const c1y = ctx.eval(this.c1y);
      if (isNaN(c1y)) return; // TODO: warn
      const c2x = ctx.eval(this.c2x);
      if (isNaN(c2x)) return; // TODO: warn
      const c2y = ctx.eval(this.c2y);
      if (isNaN(c2y)) return; // TODO: warn
      dst.cube(c1x, c1y, c2x, c2y, x, y);
    },
    "line": function(this: PathCmdElement, dst, ctx) {
      const x = ctx.eval(this.x);
      if (isNaN(x)) return; // TODO: warn
      const y = ctx.eval(this.y);
      if (isNaN(y)) return; // TODO: warn
      dst.line(x, y);
    },
    "move": function(this: PathCmdElement, dst, ctx) {
      const x = ctx.eval(this.x);
      if (isNaN(x)) return; // TODO: warn
      const y = ctx.eval(this.y);
      if (isNaN(y)) return; // TODO: warn
      dst.move(x, y);
    },
    "quad": function(this: PathCmdElement, dst, ctx) {
      const x = ctx.eval(this.x);
      if (isNaN(x)) return; // TODO: warn
      const y = ctx.eval(this.y);
      if (isNaN(y)) return; // TODO: warn
      const c1x = ctx.eval(this.c1x);
      if (isNaN(c1x)) return; // TODO: warn
      const c1y = ctx.eval(this.c1y);
      if (isNaN(c1y)) return; // TODO: warn
      dst.quad(c1x, c1y, x, y);
    }
  };

  constructor (src: RawElement) {
    super();
    if (src.name in PathCmdElement.drawFns) {
      this.draw = PathCmdElement.drawFns[src.name].bind(this);
    } else {
      return;
    }
    const attrs: RawAttributes = src.attributes || {};
    if (src.name !== 'close') {
      this.x = '' + (attrs.x || '');
      this.y = '' + (attrs.y || '');
    }
    if (src.name === 'quad') {
      this.c1x = '' + (attrs.cx || '');
      this.c1y = '' + (attrs.cy || '');
    }
    if (src.name === 'cube') {
      this.c1x = '' + (attrs.c1x || '');
      this.c1y = '' + (attrs.c1y || '');
      this.c2x = '' + (attrs.c2x || '');
      this.c2y = '' + (attrs.c2y || '');
    }
  }
}

class RectElement extends BaseElement {
  paint: PaintElement[] = [];
  x1: string;
  y1: string;
  x2: string;
  y2: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x1 = '' + (attrs.x1 || '');
    this.y1 = '' + (attrs.y1 || '');
    this.x2 = '' + (attrs.x2 || '');
    this.y2 = '' + (attrs.y2 || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof PaintElement) {
      this.paint.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const x1 = ctx.eval(this.x1);
    if (isNaN(x1)) return; // TODO: warn
    const y1 = ctx.eval(this.y1);
    if (isNaN(y1)) return; // TODO: warn
    const x2 = ctx.eval(this.x2);
    if (isNaN(x2)) return; // TODO: warn
    const y2 = ctx.eval(this.y2);
    if (isNaN(y2)) return; // TODO: warn
    dst.begin();
    dst.move(x1, y1);
    dst.line(x2, y1);
    dst.line(x2, y2);
    dst.line(x1, y2);
    dst.close();
    for (let i = 0; i < this.paint.length; i++) {
      this.paint[i].draw(dst, ctx);
    }
    dst.end();
  }
}

interface DataPoint {
  eof: boolean;
  index: number;
  raw: { [field: string]: string; };
  values: { [field: string]: number; };
}

interface ParseFunc {
  (): DataPoint;
}

class SeriesElement extends BaseElement {
  name: string;
  fields: { [field: string]: number; } = {};
  raw: string[][] = [];
  values: number[][] = [];

  private firstRawIndex: number[] = [];
  private lastRawIndex: number[] = [];
  private firstValueIndex: number[] = [];
  private lastValueIndex: number[] = [];

  static formatParserMap: { [format: string]: (src: RawElement) => ParseFunc | null; } = {
    'csv': SeriesElement.parseCSV,
    'json': SeriesElement.parseJSON,
    'xml': SeriesElement.parseXML
  };

  constructor(src: RawElement) {
    super();
    const attrs = src.attributes || { format: 'xml' };
    if (typeof attrs.name !== 'string' || attrs.name.trim().length === 0) {
      warn(`series element missing required name attribute`);
      return;
    }
    this.name = attrs.name.trim();
    let parse: ParseFunc | null = (SeriesElement.formatParserMap[attrs.format] || SeriesElement.parseXML)(src);
    if (parse === null) return;
    for (let point = parse(); !point.eof; point = parse()) {
      if (this.values.length > 0) {
        while (this.values[0].length <= point.index) {
          for (let fi = 0; fi < this.values.length; fi++) {
            this.raw[fi].push('');
            this.values[fi].push(NaN);
          }
        }
      }
      for (let f in point.values) {
        const field = f.replace(/[ \t\b\r]/g, '_');
        const seen = field in this.fields;
        if (!seen) {
          this.fields[field] = this.values.length;
          const raw: string[] = [];
          const values: number[] = [];
          for (let vi = 0; vi <= point.index; vi++) {
            raw.push('');
            values.push(NaN);
          }
          this.raw.push(raw);
          this.values.push(values);
        }
        const fi = this.fields[field];
        const vi = point.index;
        this.raw[fi][vi] = point.raw[field];
        this.values[fi][vi] = point.values[field];
        if (point.raw[field].length > 0) {
          if (this.firstRawIndex[fi] === undefined) this.firstRawIndex[fi] = vi;
          this.lastRawIndex[fi] = vi;
        }
        if (!isNaN(point.values[field])) {
          if (this.firstValueIndex[fi] === undefined) this.firstValueIndex[fi] = vi;
          this.lastValueIndex[fi] = vi;
        }
      }
    }
  }

  defineOperators(ctx: Context): void {
    const name = this.name.trim();
    if (name.length === 0 || /[ \t]/.test(name)) return;
    ctx.define(`@${name}#i`, (stack) => {
      if (this.values.length === 0) return;
      for (let vi = 0; vi < this.values[0].length; vi++) {
        stack.push(vi);
      }
    });
    for (let field in this.fields) {
      this.defineFieldOperators(ctx, field);
    }
  }

  defineFieldOperators(ctx: Context, field: string): void {
    const name = this.name.trim();
    const fi = this.fields[field];
    const raw = this.raw[fi];
    if (this.firstRawIndex[fi] !== undefined) {
      ctx.define(`@${name}#${field}:first`, raw[this.firstRawIndex[fi]]);
    }
    if (this.lastRawIndex[fi] !== undefined) {
      ctx.define(`@${name}#${field}:last`, raw[this.lastRawIndex[fi]]);
    }
    const values = this.values[fi];
    ctx.define(`@${name}#${field}`, (stack) => {
      for (let vi = 0; vi < values.length; vi++) {
        stack.push(values[vi]);
      }
    });
    ctx.define(`@${name}#${field}:first`, (stack) => {
      stack.push(this.firstValueIndex[fi] !== undefined ? values[this.firstValueIndex[fi]] : NaN);
    });
    ctx.define(`@${name}#${field}:last`, (stack) => {
      stack.push(this.lastValueIndex[fi] !== undefined ? values[this.lastValueIndex[fi]] : NaN);
    });
  }

  static parseCSV(series: RawElement): ParseFunc | null {
    if (typeof series.content !== 'string') {
      warn(`series ${name}: expected string content, but found ${typeof series.content}`);
      return null;
    }
    const parse = csv.parse(series.content.trim().replace(/^[ \t\b\r]+/gm, ''));
    const header = parse();
    if (header.length === 0) {
      warn(`series ${name}: unable to parse header line`);
      return null;
    }
    let pointIndex = 0;
    return () => {
      const point: DataPoint = {
        eof: false,
        index: pointIndex,
        raw: {},
        values: {}
      };
      const fields = parse();
      point.eof = fields.length === 0;
      if (point.eof) return point;
      for (let i = 0; i < fields.length && i < header.length; i++) {
        point.raw[header[i]] = fields[i];
        point.values[header[i]] = parseFloat(fields[i]);
      }
      pointIndex++;
      return point;
    };
  }

  static parseJSON(src: RawElement): ParseFunc | null {
    let arr = <{ [field: string]: any }>src.content;
    if (typeof src.content === 'string') {
      try {
        arr = <{ [field: string]: any }>JSON.parse(src.content);
      } catch (err) {
        warn(`series ${name}: unable to parse content as JSON: ${err}`);
        return null;
      }
    }
    if (typeof arr !== 'object' || typeof arr.length !== 'number') {
      warn(`series ${name}: unexpected content type`);
      return null;
    }
    let pointIndex = 0;
    return () => {
      const point: DataPoint = {
        eof: true,
        index: pointIndex,
        raw: {},
        values: {}
      };
      if (pointIndex >= arr.length) {
        return point;
      }
      point.eof = false;
      const item = arr[pointIndex];
      for (let key in item) {
        if (typeof item === 'object' && item.hasOwnProperty(key)) {
          const value = item[key];
          point.raw[key] = '';
          point.values[key] = NaN;
          if (typeof value === 'string') {
            point.raw[key] = value;
            point.values[key] = parseFloat(value);
          } else if (typeof value === 'number') {
            point.raw[key] = '' + value;
            point.values[key] = value;
          }
        }
      }
      pointIndex++;
      return point;
    };
  }

  static parseXML(src: RawElement): ParseFunc | null {
    const name = (<RawAttributes>src.attributes).name;
    if (typeof DOMParser === 'undefined') {
      warn(`series ${name}: unable to parse XML data (DOMParser not found)`);
      return null;
    }
    if (typeof src.content !== 'string') {
      warn(`series ${name}: expected string content, but found ${typeof src.content}`);
      return null;
    }
    const parser = new DOMParser();
    const doc = parser.parseFromString('<root>' + src.content + '</root>', 'application/xml')
    const root = doc.documentElement;
    if (!root || root.localName !== 'root') {
      warn(`series ${name}: unable to parse XML data (XML malformed)`);
      return null;
    }
    const children = root.childNodes;
    if (typeof children === 'undefined') {
      warn(`series ${name}: element has no children`);
      return null;
    }
    let pointIndex = 0;
    let i = 0;
    return () => {
      const point: DataPoint = {
        eof: true,
        index: pointIndex,
        raw: {},
        values: {}
      };
      for (i = i; i < children.length; i++) {
        const child = <Element>children[i];
        if (child.nodeType !== Node.ELEMENT_NODE) continue;
        if (child.nodeName !== 'point') {
          warn(`series ${name}: found unexpected ${child.nodeName} element`);
          continue;
        }
        if (typeof child.attributes !== 'undefined') {
          for (let i = 0; i < child.attributes.length; i++) {
            const node = child.attributes[i];
            if (typeof node.nodeValue === 'string') {
              point.raw[node.nodeName] = node.nodeValue;
              point.values[node.nodeName] = parseFloat(node.nodeValue);
            }
          }
        }
        point.eof = false;
        i++;
        break;
      }
      pointIndex++;
      return point;
    };
  }
}

class StrokeElement extends BaseElement {
  colors: ColorElement[] = [];
  x1: string;
  y1: string;
  x2: string;
  y2: string;
  width: string;
  cap: string;
  join: string;
  dash: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.x1 = '' + (attrs.x1 || '');
    this.y1 = '' + (attrs.y1 || '');
    this.x2 = '' + (attrs.x2 || '');
    this.y2 = '' + (attrs.y2 || '');
    this.width = '' + (attrs.width || '');
    this.cap = '' + (attrs.cap || '');
    this.join = '' + (attrs.join || '');
    this.dash = '' + (attrs.dash || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof ColorElement) {
      this.colors.push(child);
    }
  }

  draw(dst: Target, ctx: Context) {
    const x1 = ctx.eval(this.x1);
    if (isNaN(x1)) return;
    const y1 = ctx.eval(this.y1);
    if (isNaN(y1)) return;
    const x2 = ctx.eval(this.x2);
    if (isNaN(x2)) return;
    const y2 = ctx.eval(this.y2);
    if (isNaN(y2)) return;
    const width = ctx.eval(this.width);
    if (isNaN(width)) return;
    const cap = (this.cap === 'round' || this.cap === 'square' ? this.cap : 'butt');
    const join = (this.join === 'round' || this.join === 'bevel' ? this.join : 'miter');
    const dash = this.dash.trim();
    dst.stroke(
      {
        colors: this.colors.map((c) => c.color(ctx)),
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2
      }, width <= 0 ? 1 : width, cap, join, dash);
  }
}

class UnitsElement extends BaseElement {
  units: UnitElement[] = [];

  constructor(src: RawElement) {
    super();
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof UnitElement) {
      this.units.push(child);
    }
  }
}

class UnitElement extends BaseElement {
  name: string;
  points: UnitMapElement[] = [];

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.name = '' + (attrs.name || '');
  }

  addChild(child: BaseElement) {
    super.addChild(child);
    if (child instanceof UnitMapElement) {
      this.points.push(child);
    }
  }

  defineOperators(ctx: Context): void {
    if (this.points.length < 2) return; // TODO: warn
    const name = this.name.trim();
    if (name.length === 0 || /[ \t]/.test(name)) return; // TODO: warn
    const points: [number, number][] = [];
    for (let i = 0; i < this.points.length && i < 2; i++) {
      const point: [number, number] = [
        ctx.eval(this.points[i].unit),
        ctx.eval(this.points[i].to)
      ];
      if (isNaN(point[0]) || isNaN(point[1])) return; // TODO: warn
      points.push(point);
    }
    points.sort((a, b) => a[0] - b[0]);
    const min = points[0];
    const max = points[1];
    ctx.define(`${name}`, (stack) => {
      let value = min[1] + (stack.pop() - min[0]) / (max[0] - min[0]) * (max[1] - min[1]);
      if (!isFinite(value)) value = NaN;
      stack.push(value);
    });
    ctx.define(`!${name}`, (stack) => {
      let value = stack.pop() / (max[0] - min[0]) * (max[1] - min[1]);
      if (!isFinite(value)) value = NaN;
      stack.push(value);
    });
  }
}

class UnitMapElement extends BaseElement {
  unit: string;
  to: string;

  constructor(src: RawElement) {
    super();
    const attrs: RawAttributes = src.attributes || {};
    this.unit = '' + (attrs.unit || '');
    this.to = '' + (attrs.to || '');
  }
}

const schema: Schema = (function() {
  function mix(a: Schema, b: Schema): Schema {
    const c: Schema = {};
    for (let key in a) {
      c[key] = a[key];
    }
    for (let key in b) {
      c[key] = b[key];
    }
    return c;
  }

  function ifSchema(children: Schema): SchemaNode {
    return [IfElement, {
      "between": [IfClauseElement, children],
      "defined": [IfClauseElement, children],
      "equal": [IfClauseElement, children],
      "greater": [IfClauseElement, children],
      "less": [IfClauseElement, children],
      "undefined": [IfClauseElement, children]
    }];
  }

  const paint: Schema = {};
  paint["fill"] = [FillElement, {
    "color": [ColorElement, {}]
  }];
  paint["stroke"] = [StrokeElement, {
    "color": [ColorElement, {}]
  }];
  paint["if"] = ifSchema(paint);

  const pathCmds: Schema = {
    "close": [PathCmdElement, {}],
    "cube": [PathCmdElement, {}],
    "line": [PathCmdElement, {}],
    "move": [PathCmdElement, {}],
    "quad": [PathCmdElement, {}]
  };
  pathCmds["if"] = ifSchema(pathCmds);
  pathCmds["loop"] = [LoopElement, pathCmds];

  const shapes: Schema = {
    "arc": [ArcElement, {
      "paint": [PaintElement, paint]
    }],
    "label": [LabelElement, {
      "paint": [PaintElement, paint]
    }],
    "line": [LineElement, {
      "paint": [PaintElement, paint]
    }],
    "path": [PathElement, mix(pathCmds, {
      "paint": [PaintElement, paint]
    })],
    "rect": [RectElement, {
      "paint": [PaintElement, paint]
    }]
  };
  shapes["clip"] = [ClipElement, shapes];
  shapes["if"] = ifSchema(shapes);
  shapes["loop"] = [LoopElement, shapes];

  return mix(shapes, {
    "paint": [PaintElement, paint],
    "data": [DataElement, {
      "series": [SeriesElement, {}]
    }],
    "units": [UnitsElement, {
      "unit": [UnitElement, {
        "map": [UnitMapElement, {}]
      }]
    }]
  });
})();
