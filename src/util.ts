export function clamp(n: number, min: number, max: number): number {
  return n < min ? min : n > max ? max : n;
}

export function warn(msg: string) {
  if (typeof console.warn === 'function') {
    console.warn(msg);
  } else {
    console.log('[warn] ' + msg);
  }
}
