import { clamp } from './util';

export interface Target {
  arc(x: number, y: number, r: number, a1: number, a2: number): void;
  begin(): void;
  clip(): void;
  close(): void;
  cube(c1x: number, c1y: number, c2x: number, c2y: number, x: number, y: number): void;
  end(): void;
  fill(g: Gradient): void;
  height(): number;
  line(x: number, y: number): void;
  move(x: number, y: number): void;
  quad(cx: number, cy: number, x: number, y: number): void;
  stroke(g: Gradient, width: number, cap: 'butt' | 'round' | 'square', join: 'miter' | 'round' | 'bevel', dash: string): void;
  text(s: string, x: number, y: number, align: 'center' | 'left' | 'right', font: string, size: number, style: 'normal' | 'italic' | 'oblique', weight: number, rotate: number): void;
  width(): number;
}

export interface Gradient {
  colors: Color[];
  x1: number;
  y1: number;
  x2: number;
  y2: number;
}

export interface Color {
  r: number;
  g: number;
  b: number;
  a: number;
  offset: number;
}

interface TextChunk {
  s: string;
  x: number;
  y: number;
  align: string;
  font: string;
  rotate: number;
}

export class CanvasTarget implements Target {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  w: number;
  h: number;
  ratio: number;

  private pendingTexts: TextChunk[] = [];

  constructor(el: HTMLCanvasElement) {
    this.canvas = el;
    this.w = this.canvas.width;
    this.h = this.canvas.height;
    this.ratio = window.devicePixelRatio || 1;
    this.canvas.width = this.w * this.ratio;
    this.canvas.height = this.h * this.ratio;
    this.canvas.style.width = `${this.w}px`;
    this.canvas.style.height = `${this.h}px`;
    const ctx = el.getContext('2d');
    if (ctx === null) {
      throw new Error('sdvml: unable to acquire 2D rendering context');
    }
    this.ctx = ctx;
  }

  arc(x: number, y: number, r: number, a1: number, a2: number) {
    this.ctx.arc(this.ratio * x, this.ratio * y, this.ratio * r, a1, a2, a1 > a2);
  }

  begin() {
    this.ctx.save();
    this.ctx.beginPath();
  }

  clip() {
    this.ctx.clip();
  }

  close() {
    this.ctx.closePath();
  }

  cube(c1x: number, c1y: number, c2x: number, c2y: number, x: number, y: number) {
    this.ctx.bezierCurveTo(this.ratio * c1x, this.ratio * c1y, this.ratio * c2x, this.ratio * c2y, this.ratio * x, this.ratio * y);
  }

  end() {
    this.ctx.restore();
    this.pendingTexts = [];
  }

  fill(g: Gradient) {
    this.paint(g, (style: string | CanvasGradient) => {
      this.ctx.fillStyle = style;
      this.ctx.fill('evenodd');
      for (let i = 0; i < this.pendingTexts.length; i++) {
        const t = this.pendingTexts[i];
        this.ctx.font = t.font;
        this.ctx.textAlign = t.align;
        if (t.rotate) {
          this.ctx.save();
          this.ctx.translate(this.ratio * t.x, this.ratio * t.y);
          this.ctx.rotate(t.rotate);
          this.ctx.fillText(t.s, 0, 0);
          this.ctx.restore();
        } else {
          this.ctx.fillText(t.s, this.ratio * t.x, this.ratio * t.y);
        }
      }
    });
  }

  height(): number {
    return this.h;
  }

  line(x: number, y: number) {
    this.ctx.lineTo(this.ratio * x, this.ratio * y);
  }

  move(x: number, y: number) {
    this.ctx.moveTo(this.ratio * x, this.ratio * y);
  }

  quad(cx: number, cy: number, x: number, y: number) {
    this.ctx.quadraticCurveTo(this.ratio * cx, this.ratio * cy, this.ratio * x, this.ratio * y);
  }

  stroke(g: Gradient, width: number, cap: 'butt' | 'round' | 'square', join: 'miter' | 'round' | 'bevel', dash: string) {
    this.paint(g, (style: string | CanvasGradient) => {
      this.ctx.lineWidth = this.ratio * width;
      this.ctx.lineCap = cap;
      this.ctx.lineJoin = join;
      this.ctx.setLineDash(dash.split(',').map(d => parseFloat(d.trim()) || 0));
      this.ctx.strokeStyle = style;
      this.ctx.stroke();
      for (let i = 0; i < this.pendingTexts.length; i++) {
        const t = this.pendingTexts[i];
        this.ctx.font = t.font;
        this.ctx.textAlign = t.align;
        if (t.rotate) {
          this.ctx.save();
          this.ctx.translate(t.x, t.y);
          this.ctx.rotate(t.rotate);
          this.ctx.strokeText(t.s, 0, 0);
          this.ctx.restore();
        } else {
          this.ctx.strokeText(t.s, this.ratio * t.x, this.ratio * t.y);
        }
      }
    });
  }

  text(s: string, x: number, y: number, align: 'center' | 'left' | 'right', font: string, size: number, style: 'normal' | 'italic' | 'oblique', weight: number, rotate: number = 0) {
    const w = 100 * Math.round(clamp(weight <= 0 ? 400 : weight || 400, 100, 900) / 100);
    this.pendingTexts.push({
      s: s,
      x: x,
      y: y,
      align: align,
      font: `${style} ${w} ${this.ratio * size}px ${font}`,
      rotate: rotate || 0
    });
  }

  width(): number {
    return this.w;
  }

  private paint(g: Gradient, apply: (style: string | CanvasGradient) => void) {
    if (g.colors.length === 0) {
      apply('black');
      return;
    }
    if (g.colors.length === 1) {
      const color = g.colors[0];
      apply(`rgba(${Math.round(255 * color.r)},${Math.round(255 * color.g)},${Math.round(255 * color.b)},${color.a})`)
      return;
    }
    const gradient = this.ctx.createLinearGradient(this.ratio * g.x1, this.ratio * g.y1, this.ratio * g.x2, this.ratio * g.y2);
    for (let i = 0; i < g.colors.length; i++) {
      const color = g.colors[i];
      gradient.addColorStop(color.offset, `rgba(${Math.round(255 * color.r)},${Math.round(255 * color.g)},${Math.round(255 * color.b)},${color.a})`);
    }
    apply(gradient);
  }
}
