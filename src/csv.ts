export function parse(csv: string, delim = ',', quote = '"'): () => string[] {
  const d = (delim.length > 0 ? delim[0] : ',');
  const q = (quote.length > 0 ? quote[0] : '"');
  let start = 0;
  let length = 0;
  return () => {
    length = 0;
    const fields: string[] = [];
    for (var i = start; i < csv.length; i++) {
      const c = csv[i];
      const n = (i < csv.length - 1 ? csv[i+1] : '\n');
      if (csv[start] === q) {
        if (c === q && (n === d || n === '\n' || n === '\r')) {
          fields.push(csv.substr(start + 1, length - 1).replace(/""/g, '"'))
          i++;
          start = i + 1;
          length = 0;
          continue;
        }
        length++;
        continue;
      }
      if (c === d) {
        fields.push(csv.substr(start, length))
        start = i + 1;
        length = 0;
        continue;
      }
      if (c === '\r' || c === '\n') {
        fields.push(csv.substr(start, length))
        if (c === '\r' && n === '\n') {
          i++;
          start = i + 1;
        } else if (c === '\n') {
          start = i + 1;
        }
        length = 0;
        break;
      }
      length++;
    }
    if (length > 0) {
      fields.push(csv.substr(start, length))
      start += length;
    }
    return fields;
  };
}
