import { clamp, warn } from './util';

interface OperatorFunc {
  (stack: Stack): void;
}

export class Context {
  private static readonly builtins: { [name: string]: OperatorFunc; } = {
    "+": function(stack) {
      stack.push(stack.pop() + stack.pop());
    },
    "-": function(stack) {
      stack.push(-stack.pop() + stack.pop());
    },
    "*": function(stack) {
      stack.push(stack.pop() * stack.pop());
    },
    "/": function(stack) {
      const b = stack.pop();
      const a = stack.pop();
      const q = a / b;
      stack.push(isFinite(q) ? q : NaN);
    },
    avg: function(stack) {
      let sum = 0;
      let count = 0;
      while (stack.length() > 0) {
        const n = <number>stack.pop();
        if (isFinite(n)) {
          sum += n;
          count++;
        }
      }
      const avg = sum / count;
      stack.push(count > 0 && isFinite(avg) ? avg : NaN);
    },
    ceil: function(stack) {
      stack.push(Math.ceil(stack.pop()));
    },
    cos: function(stack) {
      stack.push(Math.cos(stack.pop() * 2 * Math.PI));
    },
    deg: function(stack) {
      stack.push(stack.pop() / 360);
    },
    floor: function(stack) {
      stack.push(Math.floor(stack.pop()));
    },
    grad: function(stack) {
      stack.push(stack.pop() / 400);
    },
    log: function(stack) {
      const base = stack.pop();
      const num = stack.pop();
      const exp = Math.log(num) / Math.log(base);
      stack.push(isFinite(exp) ? exp : NaN);
    },
    max: function(stack) {
      let max = Number.NEGATIVE_INFINITY;
      while (stack.length() > 0) {
        const n = <number>stack.pop();
        if (isFinite(n) && n > max) max = n;
      }
      stack.push(isFinite(max) ? max : NaN);
    },
    min: function(stack) {
      let min = Number.POSITIVE_INFINITY;
      while (stack.length() > 0) {
        const n = <number>stack.pop();
        if (isFinite(n) && n < min) min = n;
      }
      stack.push(isFinite(min) ? min : NaN);
    },
    sin: function(stack) {
      stack.push(Math.sin(stack.pop() * 2 * Math.PI));
    },
    sum: function(stack) {
      let sum = 0;
      while (stack.length() > 0) {
        const n = <number>stack.pop();
        if (isFinite(n)) {
          sum += n;
        }
      }
      stack.push(isFinite(sum) ? sum : NaN);
    },
    tan: function(stack) {
      stack.push(Math.tan(stack.pop() * 2 * Math.PI));
    },
    pluck: function(stack) {
      const index = ~~stack.pop();
      stack.slice(index, index + 1);
    },
    pow: function(stack) {
      const exp = stack.pop();
      const base = stack.pop();
      stack.push(Math.pow(base, exp));
    },
    px: function(stack) {
      //stack.push(stack.pop());
    },
    rad: function(stack) {
      stack.push(stack.pop() / (2 * Math.PI));
    },
    slice: function(stack) {
      const end = ~~clamp(stack.pop(), 0, stack.length() - 1);
      const start = ~~clamp(stack.pop(), 0, end);
      stack.slice(start, end + 1);
    },
    top: function(stack) {
      const count = ~~clamp(stack.pop(), 0, stack.length());
      stack.slice(stack.length() - count);
    }
  };

  private ops: { [name: string]: OperatorFunc; } = {};
  private res: { [key: string]: string; } = {};

  constructor(src: Context | null = null) {
    if (src === null) {
      for (let name in Context.builtins) {
        this.ops[name] = Context.builtins[name];
      }
    } else {
      this.ops = src.operators();
      this.res = src.resources();
    }
  }

  operators(): { [name: string]: OperatorFunc; } {
    const ops: { [name: string]: OperatorFunc; } = {};
    for (let name in this.ops) {
      ops[name] = this.ops[name];
    }
    return ops;
  }

  resources(): { [key: string]: string; } {
    const res: { [key: string]: string; } = {};
    for (let key in this.res) {
      res[key] = this.res[key];
    }
    return res;
  }

  define(key: string, value: OperatorFunc | string): void {
    if (typeof value === 'function') {
      this.ops[key] = <OperatorFunc>value;
    } else if (typeof value === 'string') {
      this.res[key] = value;
    }
  }

  eval(expr: string): number {
    const e = '' + (expr || '') + ' ';
    const stack = new Stack();
    let start = 0;
    let length = 0;
    for (let i = 0; i < e.length; i++) {
      const c = e[i];
      if (length === 0) {
        start = i;
      }
      if (c !== ' ') {
        length++;
        continue;
      }
      if (length === 0) continue;
      const token = e.substr(start, length);
      length = 0;
      if (token in this.ops) {
				this.ops[token](stack);
				continue;
			}
      const n = parseFloat(token);
      if (!isNaN(n)) {
        stack.push(n);
        continue;
      }
      warn(`unknown operator or unparseable number at index ${start}: ${token}`);
      return NaN;
    }
    return stack.pop();
  }

  sub(text: string): string {
    return text.replace(/([^ \t\b\r]+)/g, (match, key) => {
      return this.res.hasOwnProperty(key) ? this.res[key] : match;
    });
  }
}

export class Stack {
  private arr: number[] = [];

  length(): number {
    return this.arr.length;
  }

  push(value: number) {
    this.arr.push(value);
  }

  pop(): number {
    if (this.arr.length === 0) return 0;
    return <number>this.arr.pop();
  }

  slice(start: number, end: number = this.arr.length) {
    this.arr = this.arr.slice(start, end);
  }
}
