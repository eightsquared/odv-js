# odv-js

`odv-js` provides a rasterizer for [ODVML](https://eightsquaredsoftware.com/odv/) documents that renders to the HTML Canvas. **This library is in a very early stage of development** and is not recommended for production use.

Feedback, bug reports, and merge requests are welcome.

## Usage

The latest version of the library is available in the following locations:

- Unminified: [https://eightsquaredsoftware.com/odv/js/latest/odv.js](https://eightsquaredsoftware.com/odv/js/latest/odv.js)
- Minified: [https://eightsquaredsoftware.com/odv/js/latest/odv.min.js](https://eightsquaredsoftware.com/odv/js/latest/odv.min.js)

Specific versions can be requested by replacing `latest` in the above URLs with the desired version. To see available versions, check the tags in this repository.

The above URLs are hosted behind a CDN and can be used directly from a `<script>` tag as follows:

```html
<script type="text/javascript" src="//eightsquaredsoftware.com/odv/js/latest/odv.min.js"></script>
```

You are, of course, free to self-host.

Including the script will create an `odv` property on the `window` object. The value of this property contains the functions needed to parse and render ODVML documents.

It is possible to include an ODVML document inside an HTML document through the `<script>` tag. By giving the tag a `type` attribute value not understood by the browser, the browser should ignore the content.

The following trivial example renders a black rectangle to the `<canvas>` element.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <script id="src" type="image/odv+xml">
      <odv><paint><fill/></paint></odv>
    </script>
    <canvas id="dst" width="320" height="240"></canvas>
    <script type="text/javascript" src="//eightsquaredsoftware.com/odv/js/latest/odv.min.js"></script>
    <script type="text/javascript">
      // Parse the content of the script tag:
      var src = odv.parse(document.getElementById('src').innerHTML);
      // Create the target using the canvas tag:
      var dst = new odv.CanvasTarget(document.getElementById('dst'));
      // Render:
      src.draw(dst);
    </script>
  </body>
</html>
```

## Building

Node.js 6.10 or higher is recommended.

After cloning this repository, use `npm i` to install all dependencies. The following scripts are defined in `package.json`:

- `compile`: Runs the TypeScript compiler, producing intermediate output in `build/`.
- `package`: Runs Rollup, producing an unminified bundle in `dist/`.
- `minify`: Runs UglifyJS, producing a minified bundle in `dist/`.

## TODO

Although functional, there is much work left to be done.

- Cross-browser testing.
- Comments. The old adage that code should be self-commenting, and that therefore comments are unnecessary, is a pack of lies. But between coding and documentation, there is limited time left to do anything, and so a thorough set of comments to aid future contributors fell to the bottom of the list.
- Unit/integration test coverage. Ideally there would exist some sort of language-independent test suite external to this repository that all rasterizer implementations can share to ensure compatibility with the specification.
- Various changes to make this library consumable as a regular Node.js module, particularly through TypeScript.
