#!/bin/sh
. "$(dirname $0)/vars.sh"
if [ -z "$AWS_S3_BUCKET" ]; then
	>&2 echo "$SCRIPT: \$AWS_S3_BUCKET is empty; exiting"
	exit 2
elif [ -z "$AWS_S3_KEY_PREFIX" ]; then
	>&2 echo "$SCRIPT: \$AWS_S3_KEY_PREFIX is empty; exiting"
	exit 2
fi
set -e
S3_VERSIONED_PREFIX="$AWS_S3_KEY_PREFIX/$TAG"
S3_LATEST_PREFIX="$AWS_S3_KEY_PREFIX/latest"
echo "$SCRIPT: checking existence of S3 key $S3_VERSIONED_PREFIX"
set +e
aws s3 ls "s3://$AWS_S3_BUCKET/$S3_VERSIONED_PREFIX/odv.js" > /dev/null
if [ "$?" -eq 0 ]; then
	>&2 echo "$SCRIPT: key exists; exiting"
	exit 1
elif [ "$?" -ne 1 ]; then
	>&2 echo "$SCRIPT: s3 ls command failed; exiting"
	exit 1
fi
set -e
echo "$SCRIPT: uploading to $AWS_S3_BUCKET"
aws s3 cp dist/odv.js "s3://$AWS_S3_BUCKET/$S3_VERSIONED_PREFIX/odv.js"
aws s3 cp dist/odv.min.js "s3://$AWS_S3_BUCKET/$S3_VERSIONED_PREFIX/odv.min.js"
aws s3 cp dist/odv.js "s3://$AWS_S3_BUCKET/$S3_LATEST_PREFIX/odv.js"
aws s3 cp dist/odv.min.js "s3://$AWS_S3_BUCKET/$S3_LATEST_PREFIX/odv.min.js"
cd $CWD
