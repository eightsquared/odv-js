#!/bin/sh
. "$(dirname $0)/vars.sh"
set -e
echo "$SCRIPT: fetching dependencies"
npm install
echo "$SCRIPT: compiling"
npm run compile
echo "$SCRIPT: packaging"
npm run package
echo "$SCRIPT: minifying"
npm run minify
cd $CWD
