#!/bin/sh
export SCRIPT=$(basename $0 .sh)
export CWD=$(pwd)
export TAG=$(git describe --tags $SHA)
cd "$(dirname $0)/.."
